   ##############################
   #####TOP SECRET DOCUMENT######
   ##############################
   ###For Authorized Eyes Only###
   ##############################

Q: How do I generate a pdf for lulu.com from a buncha docbook sources?

A: It's not as easy as it looks!  Read on:

Have the docbook sources.

If they are modular, Bill von Hagen or Circulate style, then cat them into a single document.  ie, tmp.xml...

p---------------------------------------------------------q
| $ cat docbook.header *.docbook.xml credits > mybook.xml |
b_________________________________________________________d

At this point you have two paths open to you:

1. Spend a year learning XSLT and design a beautiful stylesheet for your book.
2. Cheat.

If you're reading this, you chose option 2.  Good choice.

Now:

You need to run the xml source code through a processor.  The best tool for this job is xsltproc.  But to re-size the default PDF xsl settings, we must pass some custom parameters in the command:

p--------------------/code/--------------------------q
xsltproc --output mybook.fo \
	 --stringparam page.width 6in \
	 --stringparam page.height 9in \
	 --stringparam body.font.family Times Roman \
	 --stringparam body.font.master 8 \
	 --stringparam page.margin.inner .4in \
	 --stringparam page.margin.outer .4in \
	 --stringparam page.margin.top .2in \
	 --stringparam page.margin.bottom .2in \
	 --stringparam title.margin.left 0.25in \
	 --stringparam body.font.size 8 \
	 --stringparam chapter.autolabel 0 \
	 ./mystyle.xsl mybook.xml
b_____________________________________________________d 

But in order for that to work we need a few extra extra parameters in a file called mystyle.xsl

So open a text editor and paste these styles into it:

p--------------------/code/--------------------------q
<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="/usr/share/xml/docbook/xsl-stylesheets-1.75.2/fo/docbook.xsl"/>
<xsl:template match="title" mode="chapter.titlepage.recto.auto.mode">  
  <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format" 
            xsl:use-attribute-sets="chapter.titlepage.recto.style" 
            margin-left="{$title.margin.left}" 
            font-size="16pt" 
            font-weight="bold" 
            font-family="{$title.font.family}">
    <xsl:call-template name="component.title">
      <xsl:with-param name="node" select="ancestor-or-self::chapter[1]"/>
    </xsl:call-template>
  </fo:block>
</xsl:template>
</xsl:stylesheet>
b_____________________________________________________d 


You might need to tweak even that for style, but you're hopefully getting the idea.  At this point you're probably thinking "Heck, I should have just gone with option 1 and taken the year to learn XSLT". Funny you think so, because the same thing occured to me at this very same point.

After running the xsltproc command, you will find a mybook.fo file in the current directory.  This of course is the food we feed to fop. 

p---------------------------------------------------q
| $ fop mybook.fo mybook.pdf && okular mybook.pdf   |
b___________________________________________________d

Fop should build the book into a PDF.  It will most likely complain that it has no idea where the fonts you've asked for are.  Ignore those errors, as we'll fix them momentarily.

Okular (or whatever pdf reader you chose to use) will open and display your PDF.  Does it look mostly right? nothing too horrible about it?  If so, continue.  If not, go back and tweak the xsl.

FONTS

To fix the fonts, you need to embed the Adobe-approved, Adobe-sanctioned fonts.  I have not yet tried to simply grab a Liberation font and rename its filename to Times.ttf or whatever.  So you need to go find the actual fonts that Adobe and Lulu want you to have.  I believe the way I ended up doing this was just walking over to a Mac and grabbing the fonts from it:

HelveticaLightItalic.ttf
Times New Roman Italic.ttf
HelveLTMM
HelveticaNeue.dfont
Times New Roman.ttf
Helvetica LT MM
Times LT MM
Times.dfont
Helvetica.dfont
Times New Roman Bold Italic.ttf
TimesLTMM
HelveticaLight.ttf
Times New Roman Bold.ttf
ZapfDingbats.ttf

Place these fonts in a directory called pdf-fonts or something.

Then we need to run a ghostscript command to get these naughty little fonts into our PDF:

p--------------/code/----------------q
gs \
  -sFONTPATH=./pdf-fonts \
  -o mybook+fonts.pdf \
  -sDEVICE=pdfwrite \
  -dPDFSETTINGS=/prepress \
   mybook.pdf
b____________________________________d

This might warn you about one thing or another but more than likely all is well.


FRONT AND BACK MATTER, COPYRIGHT, TITLE PAGE, ETC.

To get a proper title page and whatever additional material you need, just use pdftk, Circulate style.

In otherwords, generate the additional pages you need for the front and back matter, including a blank page since I think I read somewhere that Lulu requires a blank first and last page.

Then use pdftk to concatenate all of it together:

pdftk \
      blank.pdf titlepage.pdf \
      blank.pdf mybook+fonts.pdf \
      blank.pdf \
      cat output mybook_myname_goldmaster.pdf 


AND THAT'S IT

And that's it.