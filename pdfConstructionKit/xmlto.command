xmlto --stringparam page.width=4.25in --stringparam page.height=6.875in --stringparam body.font.master=8 --stringparam page.margin.inner=0.15in fo tmp.xml
fop tmp.fo paperback.pdf && okular paperback.pdf
