<chapter>
<title>
The Journey Continues
</title>

<blockquote>
  <attribution>
    The Revolutionary's Catechism
  </attribution>
  <para>
    When a comrade is in danger and the question arises whether this
    comrade should be saved or not saved, the decision must not be
    arrived at on the basis of sentiment, but solely in the interests
    of the revolutionary cause. Therefore, it is necessary to weigh
    carefully the usefulness of the comrade against the expenditure of
    revolutionary forces necessary to save the comrade, and the
    decision must be made accordingly.
  </para>
</blockquote>

<para>
  The next morning I ate breakfast with the farmers, which I
  appreciated since I wasn't actually staying around to do any work
  that day, which they knew and still invited me to stay and eat.  So
  we ate breakfast, and they all gave me travel tips, including the
  fastest way to get to KCG, and also that cranks had been rumoured to
  be out and about on the main road between here and KCG, so I might
  want to stick to one of the side routes.
</para>

<para>
  "It'll be more like a hike that way, rather than a Sunday stroll,
  but I reckon it'd be safer," said the reader.
</para>

<para>
  "I heard 'bout some cranks up near WKV or someplace," I said, "But I
  ain't heard of cranks round here yet."
</para>

<para>
"A transport from KCG passed by here, day before yesterday," he said.
"That's where we heard about it."
</para>

<para>
"Any shots fired?" I asked.
</para>

<para>"Yes," the reader said.</para>

<para>"No," my host said.</para>

<para>"Maybe," someone else said.</para>

<para>
"Depends on who in the transport you asked," the reader
explained.  "Some people said they definitely heard a few shots.
Other people didn't hear nothin."
</para>

<para>
"What kinda cranks would they have been if they couldn't hit the broad
side of a transport?" my host said.  "They either wasn't cranks, or
they didn't take a shot."
</para>

<para>
It seemed logical to me.</para>

<para>
"Stranger things have happened," the reader said.</para>

<para>
That also seemed logical to me.</para>

<para>
After breakfast, I thanked everyone for their hospitality and got back
on the road.
</para>

<para>
Down by the road, just as I was leaving the property, the
meathead from the previous night, was conveniently mending the fence
which, naturally, didn't really look like it needed mending.  As I
passed, he stopped working and said, "You know where I swear I seen
you?"
</para>

<para>
"Nope," I said and kept walking.</para>

<para>
"KNY," he said, walking along the other side of the fence to keep up with me.</para>

<para>
"Yeah, I been there," I said.  "Not much, but I been there."</para>

<para>
"I think you was with a group there.  One of them militia groups, you
know what I mean?"
</para>

<para>
"'Fraid I wasn't around for the militia," I said, assuming that he
meant The Militia, which was the term most people used for the People
during the time of the Revolution.
</para>

<para>
"Well it was more of a subgroup," he conceded.  "Maybe you heard of
'em.  Children of Elektronix."
</para>

<para>
  "Sounds familiar," I said.  "Yeah, sounds very familiar.  Something
  about destroying remnants of the old world?"
</para>

<para>
"Yeah," he said.  "That was them.  You're walkin' pretty fast.  You in
a hurry?"
</para>

<para>
"Yes I am," I said, looking straight at him.  "I told you I'm
trying to get parts fixed for KBV.  As long as it takes me, that's how
long they don't get the broadcasts."
</para>

<para>
"Funny, we're not gettin' broadcasts either at this point.
Feels like some subgroup might be up to their old tricks.  Maybe the
Children of Elektronix decided the radio stations were a little too
Old World for them?"
</para>

<para>
"I don't know," I said and laughed.  "Might wanna ask around
about that, though, thanks.  Don't work too hard."
</para>

<para>
  And with that, I was past the fence.  I was hoping he wouldn't hop
  over the fence and follow me down the road until he finally tired of
  interrogating me.  Happily, he did not bother, but I could sense his
  eyes on me for quite a while as I walked on down the road.
</para>

<para>
  Continuing my journey, I felt refreshed from my stay on the farm but
  no further enlightened.  It sounded as if the tower thought it was
  sending out transmissions, or else they had stopped sending
  transmission intentionally.  No call for help had been issued, yet
  obviously broadcasts were not being received.  There was definitely
  a breach in logic there.  Could be that their commie had passed into
  the great Dev Null, and maybe nobody else there knew how to send
  messages, in which case they probably had dispatched someone on foot
  to go find a new commie.  But I knew the commies, and I knew the
  commie over in KCG well enough to know that he'd have had an
  apprentice to cover his job if anything bad happened to him.  So I
  didn't think that was it.
</para>

<para>Whatever it was, things weren't quite adding up, and I was
starting to wonder if just walking into KCG and asking them to please
fix their tower was really all it was going to take to fix the issue.
</para>

<para>The only alternative was to go back to the mountaintop, though,
and try to pick up someone else's signal on shortwave, boost that, and
re-transmit.  But that is what they know as treating the symptom and
not the disease, and it wasn't the Children of Elektronix way.
</para>

<para>Damn revolutionaries.  Spend your formative years around those
sorts, and you start picking up the best habits that you'll never be
able to shake.  The Children of Elektronix didn't settle for quick
fixes.  They didn't settle for patching up a symptom without attacking
the disease.  They wouldn't have sent a konsole message and then throw
up their hands when nothing happened, saying, "I've done everything I
can."
</para>

<para>
They were about taking what they called "direct action".  Identify the
problem, and take whatever action is required to directly affect that
problem until it is fixed.  Don't launch a campaign to work around the
problem, don't launch an awareness group to gather a large team of a
useless majority.  Just do what needs to be done.
</para>

<para>
It was not something you can leave behind once you've done it long
enough.  And when, after such a mission to take action against a
particularly large problem, you're lying on the ground with a gash
down your back that nearly sliced a third of your body clean off the
rest of you, and these hard-headed, self-important, overly-philosophic
people save your life in spite of the resources it takes to do so,
then like the scar that the cut left, their principles are embedded
within you forever.
</para>

<para>
And so, I was taking direct action, one way or another.</para>

<para>
  I stuck to the side &#34;road&#34; as advised by my hosts.  They were right
  to say that it was a different walk, entirely; the side road was
  more of a path, higher up than the main road itself, and far less
  maintained.  I could tell it was used from time to time, although I
  couldn't imagine for what.  I guess it was used by people like me,
  who didn't want to be seen on the main road for one reason or
  another, although it didn't seem there was a lot of traffic on the
  main road, either.
</para>

<para>
Just a little bit after the sun reached its highest point, I did notice a few large vans down on the main road.  I'd almost missed them at first, they were fairly well camouflaged against the dark-ish earthtones surrounding the main road, but there were people there and I usually noticed people no matter what.</para>

<para>
I stopped walking to watch.  It seemed like there were too many people
there, just a little bit too much activity around the vans than what
there should have been.  They had a tire and some tools out by one of
the vans, and a blanket on the ground.  But they weren't working on
anything.  They weren't changing a tyre or fixing the van, they were
just mulling about, aimlessly.  It didn't make sense to me, but I got
the distinct sense that these were cranks.  They didn't look like the
stereotypical cranks, whatever that looked like.  Mean, I guess.  They
looked nice enough, but they looked like they were also fake.  Like
they were trying to look nice.
</para>

<para>
They didn't all have guns, but some of them did.  And they were
formidable guns.  I figured they were probably on the lookout for
passers-by.  Whenever someone did pass, they all set about to working,
like one of the vans had broken down.  They'd flag down a transport or
some group passing by, ostensibly for help.  And then they take
everything worth anything, at gunpoint, and who knows what they do
with the humans attached to it all.
</para>

<para>
I was brought out of my thoughtful observance by the sound of
approaching feet.  It startled me to the quick, and my heart began to
race.  I hadn't felt that degree of fear and excitement in a long
while, and I didn't much care for it now.  My eyes widened to twice
their usual size, my ears were open, hair on the back of my neck was
standing up straight, I was like a wild animal.  It wasn't something I
could control, and I could taste adrenaline in my mouth.
</para>

<para>
I looked down and saw that I was holding my Springfield.  I didn't remember taking it off my back, but there it was in my hands, ready to break someone's nose, or put an extra hole through their head if necessary.
</para>

<para>
But I was backing up, off the path, into the bank, behind trees, behind the brush.  And I was crouching down and taking aim at the road, but keeping my head on a swivel and my ears perked up.  I was ready for whatever was coming, and there just wasn't any way around that.
</para>

<para>
I could hear the footsteps getting closer, but it was from
sunset direction, up the path toward me.  That seemed like the wrong
direction for a crank to be coming; they'd be coming up the mountain
side, or possibly from a low point that I hadn't reached yet further
toward the sunrise.  But not all the way from sunset.  Still, I
watched and listened.
</para>

<para>Finally, the figure appeared up over the ridge.  I couldn't make
out who or what it was, but I did notice that if it had a gun then it
wasn't a rifle, and it wasn't drawn.
</para>

<para>When it got closer, I could see who it was.  It was the handyman, from the farm.  She indeed did not have a gun drawn, but she was moving carefully and seemed alert.  It seemed like she knew that the cranks were below.  I wondered what she was up to, and whether or not she was looking for me on this path, or whether she was looking to see if I'd gone back down to the cranks, like I was one of them, and had only gone to the farm to scout it out for a raid.  I didn't blame them.</para>

<para>In fact, I'd have probably done the same thing if someone had wandered up to my mountaintop.</para>

<para>Then again, maybe the meathead had gotten her interested and suspicious of me for entirely different reasons.  I don't know exactly what anyone on GPL farmlands would have with a former Children of Elektronix member, but everyone has disagreements and differences over something.  I certainly didn't much care for the meathead's tone when he'd been talking to me. </para>

<para>I couldn't get to the handyman now, not without scaring her half out of her wits the way she did me.  And if she had a gun it probably would be drawn at that point and one of us would end up getting shot, or else we'd just make a lot of noise and then we'd both be in trouble with the cranks.  So I sat up on the hillside, resting against the tree that concealed me, watching the handyman watch the cranks.  She had a sniper scope with her, only without a sniper attached, and she was using it as a telescope to get a better look at what was going on.</para>

<para>When she was satisfied with whatever she was curious about, she put the scope away and moved on.  She kept going in the sunrise direction, which struck me as odd.  I'd expected her to turn back and return to the farm.  So either my assumption that she was scouting the cranks for any sign of me there was wrong or else she had a secondary agenda.</para>

<para>Once she was more or less out of earshot, I descended again, this time following her.  I had no desire to follow her in secret, but did so until we ourselves were well away from the cranks below.  Once I felt we were distant enough to be able to safely speak, I quickened my pace and, as I'd hoped she would, she heard me approach and turned to look.  When she saw it was she me, she looked both relieved but also substantially suspicious.</para>

<para>"Where did you just come from?" she asked.</para>

<para>"Why are you following me?"</para>

<para>"Followin' you? how was I 'sposed to know you were on the highroad, and it seems like you're followin' me."</para>

<para>"Your friends told me to take this road," I said.  "So what were you doing back there looking at that group down on the road?"</para>

<para>That clearly took her by surprise, the realization that she'd been watched without her knowledge.  Obviously if I'd been a crank, or else if I was in collaboration with the cranks as they probably had sent her to find out, she'd have been captured, or worse.</para>

<para>She repositioned herself a little, physically to mirror the glaringly obvious mental shift I'd just caused.  "OK, so we were a little curious about whether or not you knew anything about them cranks.  Just wondering if there was any connection, you know."</para>

<para>"Thought so," I said.  "Don't blame you, I'd have checked myself in your place."</para>

<para>We stood in silence for a few moments, processing, I think, whether we had any distrust remaining or if everything could be as simple as it seemed.  I didn't quite believe her about why she'd been following me, still thinking of that meathead inquisition, but for the time being, I trusted her, I decided.  The farmers had been good people, so I saw nothing out of the ordinary with them verifying that I was as innocent as I seemed.  Whoever it was who'd seen the scar on my back had probably reported that, too.  Anybody can have a scar, but when a suspicious stranger has a gash down their back and doesn't provide a friendly explanation and claims to be a simple technician, anyone would start to wonder.</para>

<para>I could tell she didn't distrust me, either.  She seemed like a pretty good judge of people, and I wondered if she'd agreed to do this reconnaissance mission more because it had to be done than because she thought it was valid.</para>

<para>She looked me over with a quick movement of her eye, and I knew she knew I was not a man, but was perhaps unclear as to whether I myself knew it.  Commies didn't volunteer information, so I didn't say anything.
</para>
<para>She said at last, "I'm going someplace in the same direction as the way you're going.  Mind if we travel together?"</para>

<para>"I don't mind."</para>

<para>"Seems like you know how to take care of yourself," she said.
</para>
<para>"Yep."</para>

<para>We walked.</para>
</chapter>
