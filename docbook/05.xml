<chapter>
<title>Children of Elektronix</title>

<blockquote>
  <attribution>
    The Revolutionary's Catechism
  </attribution>
  <para>
    The Society has no aim other than the complete liberation and
    happiness of the masses – i.e., of the people who live by their
    own labor. Convinced that their emancipation and the achievement
    of this happiness can only come about as a result of an
    all-destroying popular revolt, the Society will use all its
    resources and energy toward increasing and intensifying the evils
    and miseries of the people until at last their patience is
    exhausted and they are driven to a general uprising.
  </para>
</blockquote>

<para>
  The Children Of Elektronix is what the subgroup had called itself.
  We got broadcasted, and after that it never was really the same
  again.  And I guess that above all else is what brought it all to a
  crumbling halt.  That was what caused the cave in.  The breakdown of
  all our relationships and a cessation of harmony.  A simple
  broadcast.  Just like what we had all been fighting against in the
  first place.
</para>

<para>
  It both proved our point, and destroyed us all at the same time.
</para>

<para>
  I can't speak for the others, but one thing I can say for certain is
  that it had destroyed me.  It tore me apart, and left me to deal
  with myself.  That was something that, let's say, I'd been ignoring
  for a while.
</para>

<para>
  Choosing to become a commie and actually man a comm tower was maybe
  a little unexpected, but for me it was just something I knew I could
  do, and it gave me the opportunity to spend time alone, which I knew
  I needed.  I needed it then and I think I'd needed it up until very
  recently.
</para>

<para>Pangs of loneliness were new for me.  It wasn't something I was used to, not for a long time.  I had been perfectly happy on my mountaintop, tending my garden, tending my repeater tower, listening in on random transmissions, sitting out on my perch, watching the emptiness.</para>

<para>The call of the People was loud, though, in its absence.  There was no good reason not to be receiving comm transmissions, not even a response to my inquiries.  Something was wrong, and it was part of the job I'd taken to find out what was wrong.  I put it off for as long as I conscientiously could manage, and then the curiousity and loneliness got the better of me.</para>

<para>A cantine full of water, my Springfield rifle strapped to my back and my Smith and Wesson on my belt, and some clothes and supplies in a backpack, and I was on the road again after what seemed like an entire life alone on my mountaintop.</para>

<para>I set off toward the sunrise, down my lonely mountain, and toward the Dev Null of the signal that once was.  There had been no news of any great catastrophic event, or any signficant change, but for all I knew, I was the last person on Earth.  It was like, and I do say "like", everybody had gone and vanished into thin air, like the boy and girl arguing on the radio after the radio host took over the show again.  </para>

<para>Then again, if they'd vanished into thin air it might have been better because then at least I'd have heard them on the airwaves.  But as it was, there was nothing.  Just emptiness.</para>

<para>It was into that emptiness that I travelled for the first time, in a long time.  The land hadn't changed much from what I remembered.  Of course, for the initial few miles I assumed correctly that I was walking on land that I'd been able to monitor from my mountaintop.  But it had all stayed pretty well the same as I remembered from when I'd walked up the mountain so long ago.  No settlements had cropped up.  When I'd found the mountain, and after I'd repaired the relay tower, I had taken quite some time to ensure the road up the mountain was blocked without looking like it had been intentionally blocked.  It hadn't been easy work, but some dynamite and an axe made it possible.</para>

<para>I'd known all about explosives by then.  By that time, I'd blown up quite a wide variety of things.  I knew how to handle them, where to place them for maximum efficiency, and how far away to be so that I didn't either blow up myself or crush myself with debris.</para>

<para>
Contrary to what some might think, learning about specialized equipment that is rudimentary like explosives and guns is not difficult.  It boils down to practise.  Blow enough objects apart, and you'll know everything there is to know about how explosives work.  And in fact, you'll have more pragmatic knowledge than someone who might know how to make them.  You might not know how to make them (or, then again, you might) but you will certainly know where to place an explosive to blow off a chunk of mountain, or where to place it if you want to blow up a vehicle cleanly rather than blowing it up and creating dangerous shards of shrapnel.</para>

<para>It's the higher level things, the more abstract tools, the creative and productive ones, that are more difficult to master.  For instance, if building a solar power array from materials found in an old junkyard was easy, I don't see why people wouldn't be building them all day long.</para>

<para>The Children of Elektronix built things.  We'd built all kinds of electronic wonders, with the parts of old machines we found lying around in the old cities.  But we also destroyed things.  We'd destroyed things in many different ways.  We destroyed people, we destroyed spirit, we destroyed machine, and we destroyed history.</para>

<para>I felt those were important things to destroy.  I guess I haven't changed my mind about that.  I still think we destroyed things that needed to be destroyed.</para>

<para>But somehow we'd all ended up destroying ourselves, and each other.  Some might say that was the price of revolution.  I say it's the price of fame.  If you ever stage a revolution, do it quietly and completely.</para>

<para>Every time I see a structure from the old world still standing, in tact and overly-grandeous, I cringe and take mental note.  I think "Next revolution, be sure to blow that building to pieces first thing."</para>

<para>At some point, buildings stop being mere functional structures for shelter or containment.  They become symbols, statements of an architect's desire to be unique and outstanding, or a statement of a politician (that is what they used to call a Crank in power), or a monument to the Rich, and so on.</para>

<para>The very layout of some of the old cities was in itself a statement.  If a man is judged to be evil, and we assume every cell in his body is evil, then the inverse of that is true for cities.  The individual buildings are reprehensible, and the way the buildings stand in relation to one another is a statement of apathy and greed.  Think about this: cities were most often not designed at all, and the design that emerged was a haphazard self-serving design for the men using the city for their own profit.  The fact that there was a downtown and a City Hall (centers of local government) and expensive "premium" housing quarters nearby, and the obligatory cottage industries of expensive restaurants and entertainment places where women would be forced by way of poverty to dance naked for these lecherous men, and useless, gratuitous things like movies and music were played only for a price; these all betray the focus of every city.  The whole body that was the city was a self-sustaining monument to avarice, with the poverty-stricken people existing only to maintain it for their rich masters-who-were-not-masters.  And of course this impoverished majority was stupid enough such that they too became dependent upon the monument, such that if the city were to fall then they would all die along with its Rich owners.  So they continued to maintain their own disease.</para>

<para>Cities used to have literal undergrounds, and outskirts, to which the poor would be relegated.  I often found that a humourous historical joke, that the poor who maintained the churning cities were never actually able to dwell in the city proper.  There was a real physical separation whenever possible.  Tall, "sky scraping" buildings housed the rich.  They were in their "penthouses", on the rooftops, far above any of the menial janitors of their city.  Just like me, and my mountaintop, my Perch.  So far above and away from everything that it had ceased to exist.  Sometimes, you really believe that; you really think that because something is so far gone from your awareness, your thoughts, and your presence, that it simply must not exist any longer.  </para>

<para>But now everything was starting to exist again, because I was walking down the side of my mountain, back into the fray.  I found myself wondering what really did exist now.  Maybe nothing did, maybe I was right.  Maybe they'd all puttered out, evaporated into the radio silence.  And if that were the case, it was all one with the Godstream now, and that was one thing I'd always have as long as I had a receiver.  And me.  I'd always have myself.</para>
</chapter>
